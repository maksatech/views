<?php
namespace Maksatech\Views\Rendering;

/**
 * Interface SiteInterface
 * @package Maksatech\Views\Rendering
 */
interface SiteInterface
{
    /**
     * @return string
     */
    public function getCache(): string;

    /**
     * @return int
     */
    public function id(): int;

    /**
     * @return bool
     */
    public function glueCss(): bool;

    /**
     * @return bool
     */
    public function compressCss(): bool;

    /**
     * @return bool
     */
    public function glueJs(): bool;

    /**
     * @return bool
     */
    public function compressJs(): bool;

    /**
     * @return bool
     */
    public function downloadRemoteJs(): bool;

    /**
     * @return bool
     */
    public function downloadRemoteCss(): bool;

    /**
     * @return string
     */
    public function theme(): string;
}