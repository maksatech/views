<?php

namespace Maksatech\Views\Rendering\Assets;

use Maksatech\Core\Core;
use Exception;

abstract class Asset extends Core
{
    public const TYPE_PUBLIC = 'public';
    public const TYPE_PRIVATE = 'private';
    public const TYPE_REMOTE = 'remote';
    public const TYPE_VIEW = 'view';
    public const TYPE_RAW = 'raw';

    public const CROSS_ORIGIN_TYPE_ANONYMOUS = 'anonymous';
    public const CROSS_ORIGIN_TYPE_USE_CREDENTIALS = 'use-credentials';

    public const CROSS_ORIGIN_TYPES = [
        self::CROSS_ORIGIN_TYPE_ANONYMOUS,
        self::CROSS_ORIGIN_TYPE_USE_CREDENTIALS
    ];

    protected string $type;
    protected string $content;
    protected array $attributes;
    protected bool $glue;
    protected bool $compress;
    protected bool $downloadRemote;
    protected bool $integrity;
    protected ?string $crossOrigin;
    protected array $variables;

    /**
     * @param string $type
     * @param string $content
     * @param array $attributes
     * @param bool $glue
     * @param bool $compress
     * @param bool $downloadRemote
     * @param bool $integrity
     * @param string|null $crossOrigin
     * @param array $variables
     * @throws Exception
     */
    public function __construct(
        string $type,
        string $content,
        array  $attributes = [],
        bool   $glue = false,
        bool   $compress = false,
        bool   $downloadRemote = false,
        bool   $integrity = false,
        ?string $crossOrigin = null,
        array  $variables = []
    )
    {
        parent::__construct();

        if(!is_null($crossOrigin) && !in_array($crossOrigin, self::CROSS_ORIGIN_TYPES)) {
            throw new Exception('Asset\'s cross origin property is not valid');
        }


        $this->type = $type;
        $this->content = $content;
        $this->attributes = $attributes;
        $this->glue = $glue;
        $this->compress = $compress;
        $this->downloadRemote = $downloadRemote;
        $this->integrity = $integrity;
        $this->crossOrigin = $crossOrigin;
        $this->variables = $variables;
    }


    public function getType(): string
    {
        return $this->type;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getGlue(): bool
    {
        return $this->glue;
    }

    public function getCompress(): bool
    {
        return $this->compress;
    }

    public function getDownloadRemote(): bool
    {
        return $this->downloadRemote;
    }

    public function getVariables(): array
    {
        return $this->variables;
    }

    public function getIntegrity(): bool
    {
        return $this->integrity;
    }

    public function getCrossOrigin(): ?string
    {
        return $this->crossOrigin;
    }

    public function isPublic(): bool
    {
        return $this->type === self::TYPE_PUBLIC;
    }

    public function isPrivate(): bool
    {
        return $this->type === self::TYPE_PRIVATE;
    }

    public function isRemote(): bool
    {
        return $this->type === self::TYPE_REMOTE;
    }

    public function isView(): bool
    {
        return $this->type === self::TYPE_VIEW;
    }

    public function isRaw(): bool
    {
        return $this->type === self::TYPE_RAW;
    }

    public function getTypedUri(): string
    {
        return $this->type .':' .$this->getContent();
    }

    public function getHash(): string
    {
        return hash("sha512", $this->getTypedUri());
    }
}