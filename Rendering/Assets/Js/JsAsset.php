<?php

namespace Maksatech\Views\Rendering\Assets\Js;

use Maksatech\Views\Rendering\Assets\Asset;
use Exception;

class JsAsset extends Asset
{
    public const LOADING_TYPE_DEFAULT = 'default';

    public const LOADING_TYPE_ASYNC = 'async';

    public const LOADING_TYPE_DEFER = 'defer';

    public const LOADING_TYPES = [
        self::LOADING_TYPE_DEFAULT,
        self::LOADING_TYPE_ASYNC,
        self::LOADING_TYPE_DEFER
    ];

    protected string $loading;

    /**
     * @param string $type
     * @param string $content
     * @param array $attributes
     * @param bool $glue
     * @param bool $compress
     * @param bool $downloadRemote
     * @param bool $integrity
     * @param string|null $crossOrigin
     * @param array $variables
     * @param string $loading
     * @throws Exception
     */
    public function __construct(
        string $type,
        string $content,
        array $attributes = [],
        bool $glue = false,
        bool $compress = false,
        bool $downloadRemote = false,
        bool   $integrity = false,
        ?string $crossOrigin = null,
        array  $variables = [],
        string $loading = self::LOADING_TYPE_DEFAULT
    )
    {
        parent::__construct($type, $content, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);

        if(!in_array($loading, JsAsset::LOADING_TYPES)) {
            throw new Exception('JavaScript\'s loading property is not valid');
        }

        $this->loading = $loading;
    }

    public function getLoading(): string
    {
        return $this->loading;
    }

    public function loadingIsDefault(): bool
    {
        return $this->loading === self::LOADING_TYPE_DEFAULT;
    }

    public function loadingIsDefer(): bool
    {
        return $this->loading === self::LOADING_TYPE_DEFER;
    }

    public function loadingIsAsync(): bool
    {
        return $this->loading === self::LOADING_TYPE_ASYNC;
    }
}