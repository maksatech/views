<?php

namespace Maksatech\Views\Rendering\Assets\Css;

use Maksatech\Views\Rendering\Assets\Asset;
use Exception;

class CssAsset extends Asset
{

    /**
     * @param string $type
     * @param string $content
     * @param array $attributes
     * @param bool $glue
     * @param bool $compress
     * @param bool $downloadRemote
     * @param bool $integrity
     * @param string|null $crossOrigin
     * @param array $variables
     * @throws Exception
     */
    public function __construct(
        string $type,
        string $content,
        array $attributes = [],
        bool $glue = false,
        bool $compress = false,
        bool $downloadRemote = false,
        bool   $integrity = false,
        ?string $crossOrigin = null,
        array  $variables = []
    )
    {
        parent::__construct($type, $content, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
    }

}