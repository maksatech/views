<?php
namespace Maksatech\Views\Rendering;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use Maksatech\Core\Core;
use Exception;
use ReflectionException;
use ReflectionMethod;

/**
 * Class ThemeInstaller
 * @package Maksatech\Views\Rendering
 */
abstract class ThemeInstaller extends Theme
{

    /**
     * @param PackageEvent|null $event
     * @throws ReflectionException
     * @throws Exception
     */
    public static function init(PackageEvent $event = null): void
    {
        $config = self::getConfig();

        if($config->hasFile())
            $config->load();

        $method = new ReflectionMethod(get_class($config),'put');

        if($method->isProtected() || $method->isPrivate())
            $method->setAccessible(true);

        $method->invokeArgs($config,[[
            'public_assets_dir' => 'assets'
        ]]);

        self::cacheInit();

        $coreConfig = Core::getConfig();

        if($coreConfig->hasFile()) {
            $coreConfig->load();

            if($coreConfig->has('web_root_dir') && $config->has('public_assets_dir') && !file_exists(self::assetsDirPath()))
                mkdir(self::assetsDirPath(),0777,true);
        }

    }
}