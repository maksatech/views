<?php

namespace Maksatech\Views\Rendering;

use Maksatech\Containers\ViewsContainerInterface;
use Maksatech\Core\Core;
use Maksatech\Views\Rendering\Exceptions\ViewNotFoundException;
use Exception;

abstract class Renderable extends Core
{
    /**
     * @var SiteInterface
     */
    protected SiteInterface $site;

    /**
     * @var string
     */
    protected string $action;

    /**
     * @var ViewsContainerInterface|null
     */
    protected $viewsContainer;

    /**
     * @var string[]
     */
    protected array $defaultViewPathArray;

    /**
     * @var string
     */
    protected string $defaultViePath;

    /**
     * @param string|null $action
     * @return bool
     */
    public function actionIsset(string $action = null):bool
    {
        return ($this->action === $action);
    }

    /**
     * @param string|null $action
     */
    public function setAction(string $action = null)
    {
        $funcInf = null;

        if($action) {
            if(mb_substr($action,0,1) == '\\') {
                $action = mb_substr($action,1);
            }
            $action = explode('::', $action, 2);
            $funcInf['class'] = trim($action[0]);
            $funcInf['function'] = trim($action[1]);
        }
        else {
            $bugTrace = debug_backtrace();
            for ($i = 0; $i<count($bugTrace) && !$funcInf; $i++) {
                if($bugTrace[$i]['class'] === static::class && $bugTrace[$i]['function'] === '__construct' && $i<count($bugTrace[$i])-1 && $bugTrace[$i+1]['function'] !== '__construct') {
                    $funcInf = $bugTrace[$i+1];
                }
            }
        }

        $dirs = explode('\\',$funcInf['class']);
        $dirsBase = mb_strtolower($dirs[0]);
        $this->defaultViewPathArray = [];
        $this->defaultViePath = self::systemRootDirPath();
        $this->defaultViewPathArray[] = $dirsBase;
        $this->defaultViePath .= '/'.$dirsBase;

        $this->defaultViePath .= '/views/'.$this->site->theme();
        $this->defaultViewPathArray[] ='views';
        $this->defaultViewPathArray[] = $this->site->theme();

        $isViewDirPart = false;
        for($i = 1; $i<count($dirs); $i++){
            $dirsStrToLower = ($i === count($dirs)-1) ? mb_strtolower(explode('Controller',$dirs[$i])[0]) : mb_strtolower($dirs[$i]);

            if($isViewDirPart) {
                $this->defaultViePath .= '/'.$dirsStrToLower;
                $this->defaultViewPathArray[] = $dirsStrToLower;
            }

            if($dirsStrToLower === 'controller' || $dirsStrToLower === 'controllers') {
                $isViewDirPart = true;
            }
        }

        if($funcInf) {
            $this->defaultViePath .= '/'.$funcInf['function'].".tpl.php";
            $this->defaultViewPathArray[] = $funcInf['function'].".tpl.php";
        }
    }

    /**
     * @param ViewsContainerInterface $viewsContainer
     * @return void
     */
    public function setViewsContainer(ViewsContainerInterface $viewsContainer): void
    {
        $this->viewsContainer = $viewsContainer;
    }

    /**
     * @return ViewsContainerInterface|null
     */
    public function getViewsContainer()
    {
        return $this->viewsContainer;
    }

    /**
     * @return bool
     */
    public function hasViewsContainer(): bool
    {
        return (null !== $this->viewsContainer);
    }

    /**
     * @param string|null $view
     * @return string
     */
    protected function viewPath(string $view = null): string
    {
        $dirStr = self::systemRootDirPath();

        if(!is_null($view) && strlen($view) > 0) {
            if(mb_substr($view,0,1) != '@') {
                $dirsBase = mb_strtolower($this->defaultViewPathArray[0]);
                $dirStr .= '/'.$dirsBase;
                $dirStr .= '/views';

                for($i = 2; $i<count($this->defaultViewPathArray)-1; $i++){
                    $dirStr .= '/'.$this->defaultViewPathArray[$i];
                }

                $dirStr .= '/'.$view.'.tpl.php';
            }
            else {
                $view = mb_substr($view,1);
                $view_expl = explode('/',$view);
                $dirStr .= "/".$view_expl[0].'/views';

                for($i = 1; $i<count($view_expl); $i++) {
                    if($view_expl[$i] === '<theme>')
                        $dirStr .= "/". $this->site->theme();
                    else
                        $dirStr .= "/". $view_expl[$i];
                }

                $dirStr .= '.tpl.php';
            }
        }
        else {
            return $this->defaultViePath;
        }

        return $dirStr;
    }

    /**
     * @param string $content
     * @return string
     */
    public static function removeCommentsFromViewContent(string $content): string
    {
        $newContent  = '';

        $commentTokens = array(T_COMMENT);

        if (defined('T_DOC_COMMENT'))
            $commentTokens[] = T_DOC_COMMENT;
        if (defined('T_ML_COMMENT'))
            $commentTokens[] = T_ML_COMMENT;

        $tokens = token_get_all($content);

        foreach ($tokens as $token) {
            if (is_array($token)) {
                if (in_array($token[0], $commentTokens))
                    continue;

                $token = $token[1];
            }

            $newContent .= $token;
        }

        return $newContent;

    }

    /**
     * @param string $content
     * @return string
     */
    public static function prepareViewContent(string $content): string
    {
        $content = self::removeCommentsFromViewContent($content);
        $content = preg_replace('/\s+/', ' ', $content);
        return str_replace(PHP_EOL, ' ', $content);
    }

    /**
     * @param array $variables
     * @param string|null $view
     * @return string
     * @throws ViewNotFoundException|Exception
     */
    public function render(array $variables = [], string $view = null): string
    {
        $viewPath = $this->viewPath($view);
        if(strlen(realpath($viewPath)) > 0) {
            extract($variables, EXTR_OVERWRITE);
            ob_start();
            if($this->hasViewsContainer() && $this->getViewsContainer()->env() === 'prod') {
                $viewCachePathDirPathParent = self::storageDirPath() . '/views';
                $viewCacheDirPath = $viewCachePathDirPathParent . '/' . Core::getCacheString();

                if(!file_exists($viewCacheDirPath)) {

                    foreach (glob($viewCachePathDirPathParent. '/*') as $pathItem) {
                        if(is_dir($pathItem)) {
                            foreach(glob($pathItem . '/*') as $pathItemItem) {
                                unlink($pathItemItem);
                            }
                            rmdir($pathItem);
                        }
                        else {
                            unlink($pathItem);
                        }
                    }

                    mkdir($viewCacheDirPath, 0777, true);
                }

                $viewCachePath = $viewCacheDirPath . '/' . hash('sha256', $viewPath) . '.php';

                if(!file_exists($viewCachePath))
                    file_put_contents($viewCachePath, self::prepareViewContent(file_get_contents($viewPath)));

                require $viewCachePath;
            } else {
                require $viewPath;
            }

            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        } else {
            throw new ViewNotFoundException($viewPath);
        }
    }

    /**
     * Theme constructor.
     * @param SiteInterface $site
     * @param string|null $action
     * @param ViewsContainerInterface|null $viewsContainer
     */
    function __construct(SiteInterface $site, string $action = null, ViewsContainerInterface $viewsContainer = null)
    {
        parent::__construct();

        $this->site = $site;
        $this->setAction($action);
        $this->setViewsContainer($viewsContainer);
    }


    /**
     * @param string $text
     * @return string
     */
    public function plainText(?string $text): string
    {
        if(is_null($text)) {
            $text = '';
        }
        
        return htmlspecialchars($text, ENT_QUOTES);
    }
}