<?php
namespace Maksatech\Views\Rendering;

use Exception;
use GuzzleHttp\Client;
use Maksatech\Containers\ViewsContainerInterface;
use Maksatech\Containers\WithViewsContainerInterface;
use Maksatech\Core\Core;
use Maksatech\Core\SetGetTrait;
use Maksatech\Views\Rendering\Assets\Asset;
use Maksatech\Views\Rendering\Assets\Css\CssAsset;
use Maksatech\Views\Rendering\Assets\Js\JsAsset;
use Maksatech\Views\Rendering\Exceptions\ViewNotFoundException;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use Throwable;

/**
 * Class Theme
 * @package Maksatech\Views\Rendering
 */
class Theme extends Renderable implements WithViewsContainerInterface
{
    use SetGetTrait;

    public const TEMPLATE_DEFAULT = 'default';

    /**
     * @var string
     */
    protected static string $configNamespace = 'core';

    /**
     * @var string
     */
    protected static string $configDir = 'views/rendering';

    /**
     * @var string
     */
    protected static string $configName = 'theme';

    /**
     * @var CssAsset[]
     */
    protected array $cssList;

    /**
     * @var array[]
     */
    protected array $jsList;

    /**
     * Theme constructor.
     * @param SiteInterface $site
     * @param string|null $action
     * @param ViewsContainerInterface|null $viewsContainer
     */
    function __construct(SiteInterface $site, string $action = null, ViewsContainerInterface $viewsContainer = null)
    {
        parent::__construct($site, $action, $viewsContainer);

        $this->cssList = [];
        $this->jsList = [
            JsAsset::LOADING_TYPE_DEFAULT => [],
            JsAsset::LOADING_TYPE_ASYNC => [],
            JsAsset::LOADING_TYPE_DEFER => []
        ];
    }

    function  __destruct()
    {
        parent::__destruct();
    }

    /**
     * @param string $dir
     * @return bool
     */
    public static function deleteDirRecursive(string $dir): bool
    {
        if(file_exists($dir)) {
            $files = array_diff(scandir($dir), ['.', '..']);

            foreach ($files as $file) {
                (is_dir("$dir/$file")) ? self::deleteDirRecursive("$dir/$file") : unlink("$dir/$file");
            }

            return rmdir($dir);
        }

        return true;
    }

    /**
     * @param bool $startSlash
     * @return string
     * @throws Exception
     */
    public static function assetsDirRelativePath(bool $startSlash = true): string
    {

        $config = self::getConfig(true,true);

        if(!$config->has('public_assets_dir')) {
            throw new Exception("'public_assets_dir' property is not defined in config file");
        }

        return ($startSlash ? '/' : '') . $config->get('public_assets_dir');
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function assetsDirPath(): string
    {
        return self::webRootDirPath() . self::assetsDirRelativePath();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function siteAssetsDirPath(): string
    {
        return self::assetsDirPath() . '/site/' . $this->site->id();
    }

    /**
     * @param bool $startSlash
     * @return string
     * @throws Exception
     */
    public function siteAssetsDirRelativePath(bool $startSlash = true): string
    {
        return self::assetsDirRelativePath($startSlash) . '/site/' . $this->site->id();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function siteAssetsCachedDirPath(): string
    {
        return $this->siteAssetsDirPath() . '/' . $this->site->getCache();
    }

    /**
     * @param bool $startSlash
     * @return string
     * @throws Exception
     */
    public function siteAssetsCachedDirRelativePath(bool $startSlash = true): string
    {
        return $this->siteAssetsDirRelativePath($startSlash) . '/' . $this->site->getCache();
    }

    /**
     * @param bool $startSlash
     * @return string
     * @throws Exception
     */
    public function siteAssetsCssCachedDirRelativePath(bool $startSlash = true): string
    {
        return $this->siteAssetsCachedDirRelativePath(true) . '/css';
    }


    /**
     * @return string
     * @throws Exception
     */
    public function siteAssetsCssCachedDirPath(): string
    {
        return $this->siteAssetsCachedDirPath() . '/css';
    }

    /**
     * @param bool $startSlash
     * @return string
     * @throws Exception
     */
    public function siteAssetsJsCachedDirRelativePath(bool $startSlash = true): string
    {
        return $this->siteAssetsCachedDirRelativePath(true) . '/js';
    }


    /**
     * @return string
     * @throws Exception
     */
    public function siteAssetsJsCachedDirPath(): string
    {
        return $this->siteAssetsCachedDirPath() . '/js';
    }


    /**
     * @return void
     * @throws Exception
     */
    public function prepareSiteAssetsCachedDir(): void
    {
        if(!file_exists($this->siteAssetsCachedDirPath())) {
            self::deleteDirRecursive($this->siteAssetsDirPath());
            @mkdir($this->siteAssetsCachedDirPath(), 0777, true);
            @mkdir($this->siteAssetsCssCachedDirPath(), 0777, true);
            @mkdir($this->siteAssetsJsCachedDirPath(), 0777, true);
        }
    }

    /**
     * @param array $attributes
     * @return string
     */
    protected static function tagAttributesString(array $attributes):string
    {
        if(count($attributes) > 0) {
            $attributesString = '';

            foreach ($attributes as $attributeKey => $attributeValue) {
                if(is_numeric($attributeKey) && $attributeKey == (int)$attributeKey && $attributeKey > 0)
                    $attributesString .= ' '.$attributeKey;
                else
                    $attributesString .= ' ' . $attributeKey . '="' . $attributeValue . '"';
            }

            return $attributesString;
        }

        return "";
    }

    /**
     * @param string|array $css
     * @throws Exception
     */
    public function addCss($css)
    {
        if(!is_array($css) || array_key_exists('uri',$css))
            $css = [$css];

        $coreConfig = Core::getConfig(true,true);

        if(!$coreConfig->has('web_root_dir')) {
            throw new Exception("'web_root_dir' property is not defined in config file");
        }

        foreach ($css as $cssItem) {
            $attributes = [];
            $glue = $this->site->glueCss();
            $compress = $this->site->compressCss();
            $downloadRemote = $this->site->downloadRemoteCss();
            $integrity = false;
            $crossOrigin = null;
            $variables = [];

            if(is_array($cssItem)) {
                if(!array_key_exists('uri',$cssItem)) {
                    throw new Exception('Stylesheet file\'s uri is not defined');
                }

                if(array_key_exists('attributes',$cssItem)) {
                    if(is_array($cssItem['attributes']))
                        $attributes = $cssItem['attributes'];
                    else
                        throw new Exception('Asset\'s attributes property must be an array');
                }

                $glue = (bool)($cssItem['glue'] ?? $glue);
                $compress = (bool)($cssItem['compress'] ?? $compress);
                $downloadRemote = (bool)($cssItem['download_remote'] ?? $downloadRemote);
                $integrity = (bool)($cssItem['integrity'] ?? $integrity);
                $crossOrigin = ($cssItem['crossorigin'] ?? $crossOrigin);

                if(array_key_exists('variables', $cssItem)) {
                    if(!is_array($cssItem['variables']))
                        throw new Exception('Asset\'s variables property must be an array');

                    $variables = $cssItem['variables'];
                }

                $cssItem = $cssItem['uri'];

            }

            $cssItemExplode = explode(':',$cssItem,2);

            if(count($cssItemExplode) === 2) {
                if($cssItemExplode[0] === 'public') {

                    if(!file_exists(self::webRootDirPath() . '/' . $cssItemExplode[1])) {
                        throw new Exception('The file '.$cssItemExplode[1].' not exists');
                    }

                    $this->cssList[] = new CssAsset($cssItemExplode[0], $cssItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);

                } elseif ($cssItemExplode[0] === 'private') {
                    if(!file_exists(self::systemRootDirPath() . '/' . $cssItemExplode[1])) {
                        throw new Exception('The file ' . $cssItemExplode[1] . ' not exists');
                    }

                    $this->cssList[] = new CssAsset($cssItemExplode[0], $cssItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);

                } elseif ($cssItemExplode[0] === 'view') {
                    $this->cssList[] = new CssAsset($cssItemExplode[0], $cssItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
                } elseif ($cssItemExplode[0] === 'http' || $cssItemExplode[0] === 'https') {
                    $this->cssList[] = new CssAsset('remote', $cssItem, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
                } elseif ($cssItemExplode[0] === 'raw') {
                    $this->cssList[] = new CssAsset($cssItemExplode[0], $cssItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
                } else throw new Exception("Unknown stylesheet resource path template: '".$cssItemExplode[0]."'");
            } elseif (mb_substr($cssItem, 0, 1) === '/') {
                $this->cssList[] = new CssAsset('public', mb_substr($cssItem, 1), $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
            } elseif(mb_substr($cssItem, 0, 2) === "//") {
                $this->cssList[] = new CssAsset('remote', $cssItem, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
            } elseif (in_array(mb_substr($cssItem,0, 7), ['<style ', '<style>']) && mb_substr($cssItem, -8) === '</style>') {
                $this->cssList[] = new CssAsset('raw', $cssItem, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
            } else throw new Exception('Unknown stylesheet resource path template');
        }
    }

    /**
     * @param $js
     * @throws Exception
     */
    public function addJs($js)
    {
        if(!is_array($js) || array_key_exists('uri',$js))
            $js = [$js];

        $coreConfig = Core::getConfig(true,true);

        if(!$coreConfig->has('web_root_dir')) {
            throw new Exception("'web_root_dir' property is not defined in config file");
        }

        foreach ($js as $jsItem) {
            $attributes = [];
            $glue = $this->site->glueJs();
            $compress = $this->site->compressJs();
            $downloadRemote = $this->site->downloadRemoteJs();
            $integrity = false;
            $crossOrigin = null;
            $jsLoadingType = JsAsset::LOADING_TYPE_DEFAULT;
            $variables = [];

            if(is_array($jsItem)) {
                if(!array_key_exists('uri',$jsItem)) {
                    throw new Exception('JavaScript file\'s uri is not defined');
                }

                if(array_key_exists('attributes',$jsItem)) {
                    if(is_array($jsItem['attributes']))
                        $attributes = $jsItem['attributes'];
                    else
                        throw new Exception('Asset\'s attributes property must be an array');
                }

                $glue = (bool)($jsItem['glue'] ?? $glue);
                $compress = (bool)($jsItem['compress'] ?? $compress);
                $downloadRemote = (bool)($jsItem['download_remote'] ?? $downloadRemote);
                $integrity = (bool)($jsItem['integrity'] ?? $integrity);
                $crossOrigin = ($jsItem['crossorigin'] ?? $crossOrigin);
                $jsLoadingType = $jsItem['loading'] ?? JsAsset::LOADING_TYPE_DEFAULT;

                if(array_key_exists('variables', $jsItem)) {
                    if(!is_array($jsItem['variables']))
                        throw new Exception('Asset\'s variables property must be an array');

                    $variables = $jsItem['variables'];
                }

                $jsItem = $jsItem['uri'];

            }

            $jsItemExplode = mb_substr($jsItem, 0, 7) === '<script' ? [$jsItem] : explode(':', $jsItem,2);

            if(count($jsItemExplode) === 2) {
                if($jsItemExplode[0] === 'public') {

                    if(!file_exists(self::webRootDirPath() . '/' . $jsItemExplode[1])) {
                        throw new Exception('The file ' . $jsItemExplode[1] . ' not exists');
                    }

                    $this->jsList[$jsLoadingType][] = new JsAsset($jsItemExplode[0], $jsItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables, $jsLoadingType);

                } elseif ($jsItemExplode[0] === 'private') {
                    if(!file_exists($jsItemExplode[1])) {
                        throw new Exception('The file ' . $jsItemExplode[1] . ' not exists');
                    }

                    $this->jsList[$jsLoadingType][] = new JsAsset($jsItemExplode[0], $jsItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables, $jsLoadingType);

                } elseif ($jsItemExplode[0] === 'view') {
                    $this->jsList[$jsLoadingType][] = new JsAsset($jsItemExplode[0], $jsItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables, $jsLoadingType);
                } elseif ($jsItemExplode[0] === 'http' || $jsItemExplode[0] === 'https') {
                    $this->jsList[$jsLoadingType][] = new JsAsset('remote', $jsItem, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables, $jsLoadingType);
                } elseif ($jsItemExplode[0] === 'raw') {
                    $this->jsList[$jsLoadingType][] = new JsAsset($jsItemExplode[0], $jsItemExplode[1], $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
                } else throw new Exception("Unknown javascript resource path template: '".$jsItemExplode[0]."'");
            }
            elseif (mb_substr($jsItem,0,1) === '/') {
                $this->jsList[$jsLoadingType][] = new JsAsset('public', mb_substr($jsItem, 1), $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables, $jsLoadingType);
            } elseif(mb_substr($jsItem,0,2) === "//") {
                $this->jsList[$jsLoadingType][] = new JsAsset('remote', $jsItem, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables, $jsLoadingType);
            } elseif (in_array(mb_substr($jsItem,0, 8),['<script ', '<script>']) && mb_substr($jsItem, -9) === '</script>') {
                $this->jsList[$jsLoadingType][] = new JsAsset('raw', $jsItem, $attributes, $glue, $compress, $downloadRemote, $integrity, $crossOrigin, $variables);
            } else throw new Exception('Unknown javascript resource path template');
        }

    }

    /**
     * @param string $buffer
     * @return string
     */
    protected static function compressCss(string $buffer): string
    {
        $minifier = new CSS();
        $minifier->add($buffer);
        return $minifier->minify();
    }

    /**
     * @param string $buffer
     * @return string
     */
    protected static function compressJs(string $buffer): string
    {
        $minifier = new JS();
        $minifier->add($buffer);
        return $minifier->minify();
    }

    public function renderCssAsset(CssAsset $cssItem): void
    {
        $this->prepareSiteAssetsCachedDir();

        $coreConfig = Core::getConfig(true,true);

        if(!$coreConfig->has('web_root_dir')) {
            throw new Exception("'web_root_dir' property is not defined in config file");
        }

        $attributes = $cssItem->getAttributes();

        if($cssItem->isPublic()) {
            if($cssItem->getCompress()) {
                $pathHash = $cssItem->getHash();

                $cssFileName = 'css-' . $pathHash . '.min.css';

                if(!$this->cssFileExists($cssFileName)) {
                    $this->putCssFileContent($cssFileName, self::compressCss(file_get_contents(self::webRootDirPath() . '/' . $cssItem->getContent())));
                }



                if($cssItem->getIntegrity()) {
                    $attributes['integrity'] = $this->getIntegrity($this->cssFilePath($cssFileName));
                }

                if(!is_null($cssItem->getCrossOrigin())) {
                    $attributes['crossorigin'] = $cssItem->getCrossOrigin();
                }

                echo '<link rel="stylesheet" href="' . $this->cssFileRelativePath($cssFileName) . '"' . self::tagAttributesString($attributes) . '>' . PHP_EOL;

            } else {

                if($cssItem->getIntegrity()) {
                    $attributes['integrity'] = $this->getIntegrity(self::webRootDirPath() . '/' . $cssItem->getContent());
                }

                if(!is_null($cssItem->getCrossOrigin())) {
                    $attributes['crossorigin'] = $cssItem->getCrossOrigin();
                }

                echo '<link rel="stylesheet" href="/' . $cssItem->getContent() . '?cache=' . $this->site->getCache() . '"' . self::tagAttributesString($attributes) . '>' . PHP_EOL;
            }
        } elseif ($cssItem->isPrivate()) {
            $pathHash = $cssItem->getHash();

            if($cssItem->getCompress())
                $cssFileName = 'css-' . $pathHash . '.min.css';
            else
                $cssFileName = 'css-' . $pathHash . '.css';

            if(!$this->cssFileExists($cssFileName)) {
                if($cssItem->getCompress()) {
                    $this->putCssFileContent($cssFileName, self::compressCss(file_get_contents(self::systemRootDirPath() . '/' . $cssItem->getContent())));
                }
                else {
                    $this->putCssFileContent($cssFileName, file_get_contents(self::systemRootDirPath() . '/' . $cssItem->getContent()));
                }
            }

            if($cssItem->getIntegrity()) {
                $attributes['integrity'] = $this->getIntegrity($this->cssFilePath($cssFileName));
            }

            if(!is_null($cssItem->getCrossOrigin())) {
                $attributes['crossorigin'] = $cssItem->getCrossOrigin();
            }

            echo '<link rel="stylesheet" href="' . $this->cssFileRelativePath($cssFileName) . '"' . self::tagAttributesString($attributes) . '>' . PHP_EOL;
        } elseif ($cssItem->isRemote()) {
            if($cssItem->getDownloadRemote()) {
                $uri = $cssItem->getContent();
                if(mb_substr($uri, 0, 2) === "//") {
                    $uri = 'https:' . $uri;
                }

                $pathHash = $cssItem->getHash();
                if($cssItem->getCompress())
                    $cssFileName = 'css-' . $pathHash . '.min.css';
                else
                    $cssFileName = 'css-' . $pathHash . '.css';


                if(!$this->cssFileExists($cssFileName)) {
                    if($cssItem->getCompress()) {
                        $this->putCssFileContent($cssFileName, self::compressCss($this->remoteFileGetContents($uri)));
                    }
                    else {
                        $this->putCssFileContent($cssFileName, $this->remoteFileGetContents($uri));
                    }
                }

                if($cssItem->getIntegrity()) {
                    $attributes['integrity'] = $this->getIntegrity($this->cssFilePath($cssFileName));
                }

                if(!is_null($cssItem->getCrossOrigin())) {
                    $attributes['crossorigin'] = $cssItem->getCrossOrigin();
                }

                echo '<link rel="stylesheet" href="' . $this->cssFileRelativePath($cssFileName) . '"' . self::tagAttributesString($attributes) . '>' . PHP_EOL;

            } else {
                echo '<link rel="stylesheet" href="' . $cssItem->getContent() . '?cache=' . $this->site->getCache() . '"' . self::tagAttributesString($attributes) . '>' . PHP_EOL;
            }
        } elseif ($cssItem->isView()) {
            $pathHash = $cssItem->getHash();

            if($cssItem->getCompress())
                $cssFileName = 'css-' . $pathHash . '.min.css';
            else
                $cssFileName = 'css-' . $pathHash . '.css';

            if(!$this->cssFileExists($cssFileName)) {
                if($cssItem->getCompress()) {
                    $this->putCssFileContent($cssFileName, self::compressCss($this->render(['site' => $this->site] + $cssItem->getVariables(), $cssItem->getContent())));
                }
                else {
                    $this->putCssFileContent($cssFileName, $this->render(['site' => $this->site] + $cssItem->getVariables(), $cssItem->getContent()));
                }
            }

            if($cssItem->getIntegrity()) {
                $attributes['integrity'] = $this->getIntegrity($this->cssFilePath($cssFileName));
            }

            if(!is_null($cssItem->getCrossOrigin())) {
                $attributes['crossorigin'] = $cssItem->getCrossOrigin();
            }

            echo '<link rel="stylesheet" href="' . $this->cssFileRelativePath($cssFileName). '"' . self::tagAttributesString($attributes) . '>' . PHP_EOL;
        } elseif($cssItem->isRaw()) {
            echo $cssItem->getContent() . PHP_EOL;
        }
    }

    public function renderJsAsset(JsAsset $jsItem): void
    {
        $this->prepareSiteAssetsCachedDir();

        $coreConfig = Core::getConfig(true,true);

        if(!$coreConfig->has('web_root_dir')) {
            throw new Exception("'web_root_dir' property is not defined in config file");
        }

        $attributes = $jsItem->getAttributes();

        if($jsItem->isPublic()) {
            if($jsItem->getCompress()) {
                $pathHash = $jsItem->getHash();
                $jsFileName = 'js-' . $pathHash . '.min.js';

                if(!$this->jsFileExists($jsFileName)) {
                    $this->putJsFileContent($jsFileName, self::compressJs(file_get_contents(self::webRootDirPath() . '/' . $jsItem->getContent())));
                }

                if($jsItem->getIntegrity()) {
                    $attributes['integrity'] = $this->getIntegrity($this->jsFilePath($jsFileName));
                }

                if(!is_null($jsItem->getCrossOrigin())) {
                    $attributes['crossorigin'] = $jsItem->getCrossOrigin();
                }

                echo '<script type="text/javascript" src="' . $this->jsFileRelativePath($jsFileName) . '"' . self::tagAttributesString($attributes) . (!$jsItem->loadingIsDefault() ? ' ' . $jsItem->getLoading() : '') . '></script>' . PHP_EOL;
            }
            else {

                if($jsItem->getIntegrity()) {
                    $attributes['integrity'] = $this->getIntegrity(self::webRootDirPath() . '/' . $jsItem->getContent());
                }

                if(!is_null($jsItem->getCrossOrigin())) {
                    $attributes['crossorigin'] = $jsItem->getCrossOrigin();
                }

                echo '<script type="text/javascript" src="/' . $jsItem->getContent() . '?cache=' . $this->site->getCache() . '"' . self::tagAttributesString($attributes) . (!$jsItem->loadingIsDefault() ? ' ' . $jsItem->getLoading() : '') . '></script>' . PHP_EOL;
            }
        } elseif($jsItem->isPrivate()) {
            $pathHash = $jsItem->getHash();

            if($jsItem->getCompress())
                $jsFileName = 'js-' . $pathHash . '.min.js';
            else
                $jsFileName = 'js-' . $pathHash . '.js';

            if(!$this->jsFileExists($jsFileName)) {
                if($jsItem->getCompress()) {
                    $this->putJsFileContent($jsFileName, self::compressJs(file_get_contents(self::systemRootDirPath() . '/' . $jsItem->getContent())));
                }
                else {
                    $this->putJsFileContent($jsFileName, file_get_contents(self::systemRootDirPath() . '/' . $jsItem->getContent()));
                }
            }

            if($jsItem->getIntegrity()) {
                $attributes['integrity'] = $this->getIntegrity($this->jsFilePath($jsFileName));
            }

            if(!is_null($jsItem->getCrossOrigin())) {
                $attributes['crossorigin'] = $jsItem->getCrossOrigin();
            }

            echo '<script type="text/javascript" src="' . $this->jsFileRelativePath($jsFileName) . '"' . self::tagAttributesString($attributes) . (!$jsItem->loadingIsDefault() ? ' ' . $jsItem->getLoading() : '') . '></script>' . PHP_EOL;

        } elseif ($jsItem->isRemote()) {
            if($jsItem->getDownloadRemote()) {
                $uri = $jsItem->getContent();
                if(mb_substr($uri, 0, 2) === "//") {
                    $uri = 'https:' . $uri;
                }

                $pathHash = $jsItem->getHash();
                if($jsItem->getCompress())
                    $jsFileName = 'js-' . $pathHash . '.min.js';
                else
                    $jsFileName = 'js-' . $pathHash . '.js';


                if(!$this->jsFileExists($jsFileName)) {
                    if($jsItem->getCompress()) {
                        $this->putJsFileContent($jsFileName, self::compressJs($this->remoteFileGetContents($uri)));
                    }
                    else {
                        $this->putJsFileContent($jsFileName, $this->remoteFileGetContents($uri));
                    }
                }

                if($jsItem->getIntegrity()) {
                    $attributes['integrity'] = $this->getIntegrity($this->jsFilePath($jsFileName));
                }

                if(!is_null($jsItem->getCrossOrigin())) {
                    $attributes['crossorigin'] = $jsItem->getCrossOrigin();
                }

                echo '<script type="text/javascript" src="' . $this->jsFileRelativePath($jsFileName) . '"' . self::tagAttributesString($attributes) . (!$jsItem->loadingIsDefault() ? ' ' . $jsItem->getLoading() : '') . '></script>' . PHP_EOL;


            } else {
                echo '<script type="text/javascript" src="' . $jsItem->getContent() . '?cache=' . $this->site->getCache() . '"' . self::tagAttributesString($attributes) . (!$jsItem->loadingIsDefault() ? ' ' . $jsItem->getLoading() : '') . '></script>' . PHP_EOL;
            }
        } elseif ($jsItem->isView()) {
            $pathHash = $jsItem->getHash();

            if($jsItem->getCompress())
                $jsFileName = 'js-' . $pathHash . '.min.js';
            else
                $jsFileName = 'js-' . $pathHash . '.js';

            if(!$this->jsFileExists($jsFileName)) {
                if($jsItem->getCompress()) {
                    $this->putJsFileContent($jsFileName, self::compressJs($this->render(['site' => $this->site] + $jsItem->getVariables(), $jsItem->getContent())));
                }
                else {
                    $this->putJsFileContent($jsFileName, $this->render(['site' => $this->site] + $jsItem->getVariables(), $jsItem->getContent()));
                }
            }

            if($jsItem->getIntegrity()) {
                $attributes['integrity'] = $this->getIntegrity($this->jsFilePath($jsFileName));
            }

            if(!is_null($jsItem->getCrossOrigin())) {
                $attributes['crossorigin'] = $jsItem->getCrossOrigin();
            }

            echo '<script type="text/javascript" src="' . $this->jsFileRelativePath($jsFileName) . '"' . self::tagAttributesString($attributes) . (!$jsItem->loadingIsDefault() ? ' ' . $jsItem->getLoading() : '') . '></script>' . PHP_EOL;
        } elseif($jsItem->isRaw()) {
            echo $jsItem->getContent() . PHP_EOL;
        }
    }

    /**
     * @param Asset[] $assetList
     * @return string
     */
    protected function generateGluedId(array $assetList): string
    {
        $assetId = '';

        foreach ($assetList as $asset) {
            if(mb_strlen($assetId) > 0) {
                $assetId .= '_';
            }

            $assetId .= $asset->getTypedUri();
        }

        return hash("sha512", $assetId);
    }

    /**
     * @param Asset[] $assetList
     * @return bool
     */
    protected function getCompressGroup(array $assetList): bool
    {
        if(count($assetList) === 0) {
            return false;
        }


        foreach ($assetList as $asset) {
            if(!$asset->getCompress()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Asset[] $assetList
     * @return bool
     */
    protected function getIntegrityGroup(array $assetList): bool
    {
        if(count($assetList) === 0) {
            return false;
        }


        foreach ($assetList as $asset) {
            if(!$asset->getIntegrity()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Asset[] $assetList
     * @return ?string
     */
    protected function getCrossOriginGroup(array $assetList): ?string
    {
        if(count($assetList) === 0) {
            return null;
        }

        return current($assetList)->getCrossOrigin();
    }

    /**
     * @param JsAsset[] $jsList
     * @return string
     */
    protected function getLoadingJsGroup(array $jsList): string
    {

        if(count($jsList) > 0) {
            return current($jsList)->getLoading();
        }

        return JsAsset::LOADING_TYPE_DEFAULT;
    }

    /**
     * @param CssAsset[] $cssList
     * @return void
     * @throws ViewNotFoundException
     */
    public function renderGluedCss(array $cssList): void
    {
        $this->prepareSiteAssetsCachedDir();

        $coreConfig = Core::getConfig(true,true);

        if(!$coreConfig->has('web_root_dir')) {
            throw new Exception("'web_root_dir' property is not defined in config file");
        }

        if(count($cssList) > 0) {
            $cssFileName = $this->generateGluedId($cssList);
            $compress = $this->getCompressGroup($cssList);
            $integrity = $this->getIntegrityGroup($cssList);
            $crossOrigin = $this->getCrossOriginGroup($cssList);
            $attributes = [];

            if($compress) {
                $cssFileName = 'css-' . $cssFileName . '.min.css';
            } else {
                $cssFileName = 'css-' . $cssFileName . '.css';
            }

            if(!$this->cssFileExists($cssFileName)) {
                foreach ($cssList as $cssItem) {
                    if($cssItem->isPublic()) {
                        if(!$compress && $cssItem->getCompress()) {
                            $this->putCssFileContent($cssFileName, self::compressCss(file_get_contents(self::webRootDirPath() . '/' . $cssItem->getContent())) . PHP_EOL, FILE_APPEND);
                        } else {
                            $this->putCssFileContent($cssFileName, file_get_contents(self::webRootDirPath() . '/' . $cssItem->getContent()) . PHP_EOL, FILE_APPEND);
                        }
                    } elseif ($cssItem->isPrivate()) {

                        if(!$compress && $cssItem->getCompress()) {
                            $this->putCssFileContent($cssFileName, self::compressCss(file_get_contents(self::systemRootDirPath() . '/' . $cssItem->getContent())) . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putCssFileContent($cssFileName, file_get_contents(self::systemRootDirPath() . '/' . $cssItem->getContent()) . PHP_EOL, FILE_APPEND);
                        }

                    } elseif ($cssItem->isRemote()) {
                        $uri = $cssItem->getContent();
                        if(mb_substr($uri, 0, 2) === "//") {
                            $uri = 'https:' . $uri;
                        }

                        if(!$compress && $cssItem->getCompress()) {
                            $this->putCssFileContent($cssFileName, self::compressCss($this->remoteFileGetContents($uri)) . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putCssFileContent($cssFileName, $this->remoteFileGetContents($uri) . PHP_EOL, FILE_APPEND);
                        }

                    } elseif ($cssItem->isView()) {
                        if(!$compress && $cssItem->getCompress()) {
                            $this->putCssFileContent($cssFileName, self::compressCss($this->render(['site' => $this->site] + $cssItem->getVariables(), $cssItem->getContent())) . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putCssFileContent($cssFileName, $this->render(['site' => $this->site] + $cssItem->getVariables(), $cssItem->getContent()) . PHP_EOL, FILE_APPEND);
                        }
                    } elseif ($cssItem->isRaw()) {
                        if(!$compress && $cssItem->getCompress()) {
                            $this->putCssFileContent($cssFileName, self::compressCss(mb_substr(preg_replace('|<style[^>]*?>(.*?)|', '\1', $cssItem->getContent(), 1), 0, -8)) . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putCssFileContent($cssFileName, mb_substr(preg_replace('|<style[^>]*?>(.*?)|', '\1', $cssItem->getContent(), 1), 0, -8) . PHP_EOL, FILE_APPEND);
                        }
                    }
                }
            }

            if($compress) {
                $this->putCssFileContent($cssFileName, self::compressCss($this->getCssFileContent($cssFileName)));
            }

            if($integrity) {
                $attributes['integrity'] = $this->getIntegrity($this->cssFilePath($cssFileName));
            }

            if(!is_null($crossOrigin)) {
                $attributes['crossorigin'] = $crossOrigin;
            }

            echo '<link rel="stylesheet" href="' . $this->cssFileRelativePath($cssFileName). '"' . self::tagAttributesString($attributes) . '>' . PHP_EOL;

        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function renderCss(): void
    {
        $cssItemGroup = [];

        foreach ($this->cssList as $cssItem) {
            if(
                !$cssItem->getGlue() ||
                ($cssItem->isRemote() && !$cssItem->getDownloadRemote()) ||
                (
                    count($cssItemGroup) > 0 && ($cssItem->getIntegrity() !== $this->getIntegrityGroup($cssItemGroup) || $cssItem->getCrossOrigin() !== $this->getCrossOriginGroup($cssItemGroup))
                )
            ) {
                if(count($cssItemGroup) > 0) {
                    $this->renderGluedCss($cssItemGroup);
                    $cssItemGroup = [];
                }

                $this->renderCssAsset($cssItem);
            } else {
                $cssItemGroup[] = $cssItem;
            }
        }

        $this->renderGluedCss($cssItemGroup);
    }

    /**
     * @param JsAsset[] $jsList
     * @return void
     * @throws ViewNotFoundException
     */
    public function renderGluedJs(array $jsList): void
    {
        $this->prepareSiteAssetsCachedDir();

        $coreConfig = Core::getConfig(true,true);

        if(!$coreConfig->has('web_root_dir')) {
            throw new Exception("'web_root_dir' property is not defined in config file");
        }

        $loading = $this->getLoadingJsGroup($jsList);

        if(count($jsList) > 0) {
            $jsFileName = $this->generateGluedId($jsList);
            $compress = $this->getCompressGroup($jsList);
            $integrity = $this->getIntegrityGroup($jsList);
            $crossOrigin = $this->getCrossOriginGroup($jsList);
            $attributes = [];

            if($compress) {
                $jsFileName = 'js-' . $jsFileName . '.min.js';
            } else {
                $jsFileName = 'js-' . $jsFileName . '.js';
            }

            if(!$this->jsFileExists($jsFileName)) {
                foreach ($jsList as $jsItem) {
                    if($jsItem->isPublic()) {
                        if(!$compress && $jsItem->getCompress()) {
                            $this->putJsFileContent($jsFileName, self::compressJs(file_get_contents(self::webRootDirPath() . '/' . $jsItem->getContent()) . ';') . PHP_EOL, FILE_APPEND);
                        } else {
                            $this->putJsFileContent($jsFileName, file_get_contents(self::webRootDirPath() . '/' . $jsItem->getContent()) . ';' . PHP_EOL, FILE_APPEND);
                        }
                    } elseif ($jsItem->isPrivate()) {

                        if(!$compress && $jsItem->getCompress()) {
                            $this->putJsFileContent($jsFileName, self::compressJs(file_get_contents(self::systemRootDirPath() . '/' . $jsItem->getContent()) . ';') . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putJsFileContent($jsFileName, file_get_contents(self::systemRootDirPath() . '/' . $jsItem->getContent()) . ';' . PHP_EOL, FILE_APPEND);
                        }

                    } elseif ($jsItem->isRemote()) {
                        $uri = $jsItem->getContent();
                        if(mb_substr($uri, 0, 2) === "//") {
                            $uri = 'https:' . $uri;
                        }

                        if(!$compress && $jsItem->getCompress()) {
                            $this->putJsFileContent($jsFileName, self::compressJs($this->remoteFileGetContents($uri) . ';') . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putJsFileContent($jsFileName, $this->remoteFileGetContents($uri) . ';' . PHP_EOL, FILE_APPEND);
                        }

                    } elseif ($jsItem->isView()) {
                        if(!$compress && $jsItem->getCompress()) {
                            $this->putJsFileContent($jsFileName, self::compressJs($this->render(['site' => $this->site] + $jsItem->getVariables(), $jsItem->getContent()) . ';') . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putJsFileContent($jsFileName, $this->render(['site' => $this->site] + $jsItem->getVariables(), $jsItem->getContent()) . ';' . PHP_EOL, FILE_APPEND);
                        }
                    } elseif ($jsItem->isRaw()) {
                        if(!$compress && $jsItem->getCompress()) {
                            $this->putJsFileContent($jsFileName, self::compressJs(mb_substr(preg_replace('|<script[^>]*?>(.*?)|', '\1', $jsItem->getContent(), 1), 0, -9) . ';') . PHP_EOL, FILE_APPEND);
                        }
                        else {
                            $this->putJsFileContent($jsFileName, mb_substr(preg_replace('|<script[^>]*?>(.*?)|', '\1', $jsItem->getContent(), 1), 0, -9) . ';' . PHP_EOL, FILE_APPEND);
                        }
                    }
                }
            }

            if($compress) {
                $this->putJsFileContent($jsFileName, self::compressJs($this->getJsFileContent($jsFileName)));
            }

            if($integrity) {
                $attributes['integrity'] = $this->getIntegrity($this->jsFilePath($jsFileName));
            }

            if(!is_null($crossOrigin)) {
                $attributes['crossorigin'] = $crossOrigin;
            }

            echo '<script type="text/javascript" src="' . $this->jsFileRelativePath($jsFileName) . '"' . self::tagAttributesString($attributes) . ($loading !== JsAsset::LOADING_TYPE_DEFAULT ? ' ' . $loading : '') . '></script>' . PHP_EOL;

        }

    }

    /**
     * @param JsAsset[] $jsList
     * @return void
     * @throws ViewNotFoundException
     */
    public function renderGroupJs(array $jsList): void
    {
        $jsItemGroup = [];

        foreach ($jsList as $jsItem) {
            if(
                !$jsItem->getGlue() ||
                ($jsItem->isRemote() && !$jsItem->getDownloadRemote()) ||
                (
                    count($jsItemGroup) > 0 && ($jsItem->getIntegrity() !== $this->getIntegrityGroup($jsItemGroup) || $jsItem->getCrossOrigin() !== $this->getCrossOriginGroup($jsItemGroup))
                )
            ) {
                if(count($jsItemGroup) > 0) {
                    $this->renderGluedJs($jsItemGroup);
                    $jsItemGroup = [];
                }

                $this->renderJsAsset($jsItem);
            } else {
                $jsItemGroup[] = $jsItem;
            }
        }

        $this->renderGluedJs($jsItemGroup);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function renderJs(): void
    {
        foreach ($this->jsList as $jsLoadingType => $jsSubList) {

            if($jsLoadingType === JsAsset::LOADING_TYPE_ASYNC) {
                foreach ($jsSubList as $jsItem) {
                    $this->renderJsAsset($jsItem);
                }
            } else {
                $this->renderGroupJs($jsSubList);
            }
        }
    }

    /**
     * @param string $name
     * @return string
     * @throws ViewNotFoundException
     */
    public function getSvg(string $name): string
    {
        $this->prepareSiteAssetsCachedDir();

        $coreConfig = Core::getConfig(true,true);

        if(!$coreConfig->has('web_root_dir')) {
            throw new Exception("'web_root_dir' property is not defined in config file");
        }

        if(!file_exists($this->siteAssetsCachedDirPath().'/images/svg/'.$name.'.svg')) {

            if(!file_exists($this->siteAssetsCachedDirPath().'/images/svg')) {
                mkdir($this->siteAssetsCachedDirPath() . '/images/svg', 0777, true);
            }


            file_put_contents($this->siteAssetsCachedDirPath().'/images/svg/'.$name.'.svg', $this->render([
                'main_bg_color' => $this->site->get('main_bg_color'),
                'main_bg_color2' => $this->site->get('main_bg_color2'),
                'main_bg_color3' => $this->site->get('main_bg_color3'),
                'main_font_color' => $this->site->get('main_font_color'),
                'main_font_color2' => $this->site->get('main_font_color2'),
                'main_link_color' => $this->site->get('main_link_color')
            ],'@app/'.$this->site->theme().'/page/page/svg/'.$name));
        }

        return $this->siteAssetsCachedDirRelativePath().'/images/svg/'.$name.'.svg';
    }

    protected function cssFileExists(string $cssFileName): bool
    {
        return file_exists($this->cssFilePath($cssFileName));
    }

    protected function cssFilePath(string $cssFileName): string
    {
        return $this->siteAssetsCssCachedDirPath() . '/' . $cssFileName;
    }

    protected function cssFileRelativePath(string $cssFileName): string
    {
        return $this->siteAssetsCssCachedDirRelativePath() . '/' . $cssFileName;
    }

    protected function putCssFileContent(string $cssFileName, string $content, int $flags = 0): void
    {
        file_put_contents($this->cssFilePath($cssFileName), $content, $flags);
    }

    protected function getCssFileContent(string $cssFileName): string
    {
        return file_get_contents($this->cssFilePath($cssFileName));
    }

    protected function compressCssFile(string $cssFileName): void
    {
        $cssFilePath = $this->cssFilePath($cssFileName);
        file_put_contents($cssFilePath,self::compressCss(file_get_contents($cssFilePath)));
    }

    protected function jsFileExists(string $jsFileName): bool
    {
        return file_exists($this->jsFilePath($jsFileName));
    }

    protected function jsFilePath(string $jsFileName): string
    {
        return $this->siteAssetsJsCachedDirPath() . '/' . $jsFileName;
    }

    protected function jsFileRelativePath(string $jsFileName): string
    {
        return $this->siteAssetsJsCachedDirRelativePath() . '/' . $jsFileName;
    }

    protected function putJsFileContent(string $jsFileName, string $content, int $flags = 0): void
    {
        file_put_contents($this->jsFilePath($jsFileName), $content, $flags);
    }

    protected function getJsFileContent(string $jsFileName): string
    {
        return file_get_contents($this->jsFilePath($jsFileName));
    }

    protected function compressJsFile(string $jsFileName): void
    {
        $jsFilePath = $this->jsFilePath($jsFileName);
        file_put_contents($jsFilePath,self::compressJs(file_get_contents($jsFilePath)));
    }

    protected function getIntegrity(string $filePath): string
    {
        return 'sha384-' . base64_encode(hash_file('sha384', $filePath, true));
    }

    protected function remoteFileGetContents(string $url): string
    {
        try {
            $client = new Client();
            return $client->get($url)->getBody()->getContents();

        } catch (Throwable $e) {
            return '';
        }
    }
}