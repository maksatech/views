<?php

namespace Maksatech\Views\Rendering;

use Maksatech\Core\Core;
use Maksatech\Containers\SiteInterface as ContainersSiteInterface;
use Maksatech\Core\SetGetTrait;

class UnknownSite implements SiteInterface, ContainersSiteInterface
{
    use SetGetTrait;

    public function id(): int {
        return 0;
    }

    public function glueJs(): bool
    {
        return Core::envType() === 'prod';
    }

    public function compressCss(): bool
    {
        return Core::envType() === 'prod';
    }

    public function glueCss(): bool
    {
        return Core::envType() === 'prod';
    }

    public function compressJs(): bool
    {
        return Core::envType() === 'prod';
    }

    public function downloadRemoteJs(): bool
    {
        return Core::envType() === 'prod';
    }

    public function downloadRemoteCss(): bool
    {
        return Core::envType() === 'prod';
    }

    public function theme(): string
    {
        return Theme::TEMPLATE_DEFAULT;
    }

    public function getCache(): string
    {
        return Core::getCacheString();
    }

    public static function loadByDomain(string $domain): ?ContainersSiteInterface
    {
        return new static();
    }
}