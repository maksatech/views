<?php

namespace Maksatech\Views\Rendering\Exceptions;


use Throwable;
use Exception;

/**
 * Class ViewNotFoundException
 * @package Maksatech\Views\Rendering\Exceptions
 */
class ViewNotFoundException extends Exception
{
    /**
     * ViewNotFoundException constructor.
     * @param $viewPath
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($viewPath, $code = 0, Throwable $previous = null)
    {
        parent::__construct('View "'.$viewPath.'" not found', $code, $previous);
    }
}